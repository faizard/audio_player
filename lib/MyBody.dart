import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class MyBody extends StatefulWidget {
  const MyBody({Key? key}) : super(key: key);

  @override
  _MyBodyState createState() => _MyBodyState();
}

class _MyBodyState extends State<MyBody> {
  AudioPlayer audioPlayer = new AudioPlayer();
  Duration duration = new Duration();
  Duration position = new Duration();

  bool playing = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(40),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            // Text("Oke siapa aja bisa "),
            // Image.network(
            //     "https://cdn-icons-png.flaticon.com/512/3043/3043665.png"),
            slider(),
            InkWell(
              onTap: getAudio,
              child: Icon(
                playing
                    ? Icons.play_circle_fill_outlined
                    : Icons.stop_circle_outlined,
                size: 100,
                color: Colors.blue,
              ),
            ),
            // ElevatedButton(onPressed: getAudio, child: Text("Mulai"))
          ],
        ),
      ),
    );
  }

  Widget slider() {
    return Slider.adaptive(
        min: 0,
        max: 100000,
        value: duration.inMicroseconds.toDouble(),
        onChanged: (e) {
          print(e);
        });
  }

  void getAudio() async {
    var url = "http://sci.streamingmurah.com:8281/stream";

    if (playing) {
      // pause
      var res = await audioPlayer.pause();
      if (res == 1) {
        setState(() {
          playing = false;
        });
      }
    } else {
      // playy

      var res = await audioPlayer.play(url, isLocal: true);
      print(res);

      if (res == 1) {
        setState(() {
          playing = true;
        });
      }
    }
  }
}
